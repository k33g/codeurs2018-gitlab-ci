# codeurs2010-gitlab-ci

**[👋 Slides](https://gitlab.com/k33g/codeurs2018-gitlab-ci/blob/master/Codeurs-En-Seine-GitLab-CI.pdf)**

- Poser des questions sur la présentation: [https://gitlab.com/k33g/codeurs2018-gitlab-ci/issues](https://gitlab.com/k33g/codeurs2018-gitlab-ci/issues)
- Poser d'autres questions: [https://gitlab.com/k33g/q/issues](https://gitlab.com/k33g/q/issues)
- Vagrant files pour installer: [https://gitlab.com/tanuki-tools/vagrant-files](https://gitlab.com/tanuki-tools/vagrant-files)
- Starter-kit pour Clever Cloud: [https://gitlab.com/CleverCloud/starter-kits](https://gitlab.com/CleverCloud/starter-kits)

> Les exemples de scripts CI utilisés pour la présentation:
> - Calculette: `.calculette-gitlab-ci.yml`
> - OpenFaaS: `.openfaas-gitlab-ci.yml`
> - Clever Cloud: `.clever-cloud-gitlab-ci.yml`


